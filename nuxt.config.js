import axios from 'axios'

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  //   ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Runtime configs
  publicRuntimeConfig: {
    apiUrl: process.env.BACKEND_URL,
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    htmlAttrs: {
      lang: 'fr',
    },
    title: 'ohoui.org',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'La compagnie Oh ! Oui… invente un théâtre résolument musical, où la vitalité d’interprètes comédiens-chanteurs-musiciens donne aux spectacles l’élan d’un concert.',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/scss/main.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // api factory
    { src: '~/plugins/api.js' },
    // enable preview mode
    { src: '~/plugins/preview.client.js' },

    // use strings from backend for static content (button labels, form messages etc.)
    { src: '~/plugins/i18n.js' },

    // vimeo player
    {
      src: '~/plugins/vue-video-background',
      ssr: false,
    },

    // vuelidate
    { src: '~/plugins/vuelidate.js' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    // https://image.nuxtjs.org/
    '@nuxt/image',
    // Doc: https://github.com/nuxt-community/style-resources-module#readme
    // '@nuxtjs/style-resources',
    // https://github.com/nuxt-community/moment-module#readme
    '@nuxtjs/moment',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',

    // https://github.com/rigor789/vue-scrollto#readme
    ['vue-scrollto/nuxt', { easing: [0.83, 0, 0.17, 1], duration: 800 }],
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: { baseURL: process.env.BACKEND_URL },

  //   image: {
  //     domains: ['https://ohoui-backend-j323n.ondigitalocean.app/'],
  //   },
  //   directus image provider for nuxt-image
  image: {
    provider: 'directus',
    isStatic: true,
    domains: ['admin.ohoui.org'],
    staticFilename: '[publicPath]/images/[name]-[hash][ext]',
    providers: {
      directus: {
        provider: require.resolve('./providers/directus'),
        options: {
          //   baseURL: `${process.env.BACKEND_URL}/assets/`,
        },
      },
    },
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
  // configuration for static site generation
  generate: {
    async routes() {
      //   return axios
      //     .get(
      //       `${process.env.BACKEND_URL}/items/shows?fields=*.*,*.directus_files_id.*`
      //     )
      //     .then((res) => {
      //       return res.data.data.map((show) => {
      //         return {
      //           route: '/spectacles/' + show.slug,
      //           payload: show,
      //         }
      //       })
      //     })

      const pages = await axios.get(
        `${process.env.BACKEND_URL}/items/pages?fields=*.*`
      )

      const shows = await axios.get(
        `${process.env.BACKEND_URL}/items/shows?fields=*.*,*.directus_files_id.*`
      )

      const pageRoutes = pages.data.data.map((page) => {
        const slug = page.slug ? page.slug : ''
        console.log(`/${slug}`)
        return {
          route: `/${slug}`,
          payload: page,
        }
      })

      const showRoutes = shows.data.data.map((show) => {
        console.log(`/spectacles/${show.slug}`)
        return {
          route: `/spectacles/${show.slug}`,
          payload: show,
        }
      })

      const routes = pageRoutes.concat(showRoutes)

      return routes
    },
  },
}
