const fetch = require('node-fetch')
const { ENV, SENDINBLUE_API_KEY, SENDINBLUE_INSCRIPTION_LIST_IDS } = process.env
// Domains to whitelist.
const originWhitelist = [] // keep this empty and append domains to whitelist using whiteListDomain()

// only for local developement
if (ENV === 'development') {
  whitelistDomain('localhost:8888')
  whitelistDomain('localhost:3000')
}

whitelistDomain('ohoui.netlify.app')
whitelistDomain('ohoui.org')

function whitelistDomain(domain, addWww = true) {
  const prefixes = ['https://', 'http://']
  if (addWww) {
    prefixes.push('https://www.')
    prefixes.push('http://www.')
  }
  prefixes.forEach((prefix) => originWhitelist.push(prefix + domain))
}

// const headers = {
//   // better change this for production
//   'Access-Control-Allow-Origin': '*',
//   // 'Access-Control-Allow-Origin':
//   //   'https://piaget-drive-to-store-40b752.netlify.app',
//   // 'Access-Control-Allow-Origin': isOriginWhitelisted
//   //   ? origin
//   //   : originWhitelist[0],
//   'Access-Control-Allow-Methods': 'POST',
//   'Access-Control-Allow-Headers': 'Content-Type',
// }
//

exports.handler = async (event, context, callback) => {
  // parse the body to JSON so we can use it in JS
  const payload = JSON.parse(event.body)

  const origin = event.headers['origin'] || event.headers['Origin'] || ''
  console.log(`Received ${event.httpMethod} request from, origin: ${origin}`)

  const isOriginWhitelisted = originWhitelist.indexOf(origin) >= 0

  console.log(
    'originWhitelist.indexOf(origin) >= 0',
    originWhitelist.indexOf(origin) >= 0
  )

  const headers = {
    // 'Access-Control-Allow-Origin': '*', // allow all domains to POST. Use for localhost development only
    'Access-Control-Allow-Origin': isOriginWhitelisted
      ? origin
      : originWhitelist[0],
    'Access-Control-Allow-Methods': 'POST',
    'Access-Control-Allow-Headers': 'Content-Type',
  }

  // only allow POST requests
  if (event.httpMethod !== 'POST') {
    return {
      statusCode: 422,
      body: JSON.stringify({
        message: 'Method Not Allowed',
      }),
    }
  }

  // validate the form
  //   if (!payload.name || !payload.firstname || !payload.email) {
  if (!payload.email) {
    return {
      statusCode: 410,
      headers,
      body: JSON.stringify({
        message: 'Required information is missing.',
      }),
    }
  }
  if (isOriginWhitelisted) {
    // all checks passed, send the mail
    console.log('try sending')

    // configure sendinblue list IDs Array
    const listIDsArr = SENDINBLUE_INSCRIPTION_LIST_IDS.split(',').map((item) =>
      Number(item.trim())
    )

    const url = 'https://api.sendinblue.com/v3/contacts'
    const options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'api-key': SENDINBLUE_API_KEY,
      },
      body: JSON.stringify({
        updateEnabled: false,
        attributes: {
          PRENOM: payload.firstname,
          NOM: payload.name,
        },
        listIds: listIDsArr,
        email: payload.email,
      }),
    }
    const answer = await fetch(url, options)
      .then((res) => res.json())

      .then((json) => {
        return json
      })
      .catch((err) => {
        return err
      })
    return {
      statusCode: 200,
      body: JSON.stringify(answer),
    }
  } else {
    return {
      statusCode: 200,
      body: 'invalid domain',
    }
  }
}
