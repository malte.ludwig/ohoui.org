export const state = () => ({
  pages: {},
})

export const mutations = {
  set(state, data) {
    state.pages = data
  },
}
export const actions = {}
export const getters = {
  pages: (state) => state.pages,
  topPages: (state) =>
    state.pages.filter((page) => {
      return !page.parent_page
    }),
}
