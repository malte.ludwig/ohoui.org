export const state = () => ({
  data: {},
})

export const mutations = {
  set(state, data) {
    const items = data.items
    const output = {}
    items.forEach((element) => {
      output[element.key] = element.value
    })
    state.data = data.items
  },
}
export const actions = {}
export const getters = {
  key: (state) => (id) => {
    for (const index in state.data) {
      const item = state.data[index]
      if (state.data[index].key === id) return item.value
    }
  },
}
