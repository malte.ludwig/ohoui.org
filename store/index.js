export const actions = {
  async nuxtServerInit({ commit }, { app }) {
    const shows = await app.$api.show.getShowList()
    commit('shows/set', shows.data)

    const translations = await app.$api.app.getTranslations()
    commit('i18n/set', translations.data)

    const navigation = await app.$api.app.getNavigation()
    commit('navigation/set', navigation.data)
  },
}
