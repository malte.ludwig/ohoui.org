export const state = () => ({
  items: null,
  itemsTransition: null,
})

export const mutations = {
  setItems(state, items) {
    state.items = items
  },
}
export const actions = {}
export const getters = {
  items: (state) => state.items,
}
