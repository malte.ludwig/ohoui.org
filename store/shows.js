export const state = () => ({
  items: null,
})

export const mutations = {
  set(state, items) {
    state.items = items
  },
}
export const actions = {}
export const getters = {
  shows: (state) => state.items,
}
