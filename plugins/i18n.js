export default (context, inject) => {
  // Initialize API factories
  const tr =
    ('tr',
    (string) => {
      return context.app.store.getters['i18n/key'](string)
    })

  // Inject $t
  inject('tr', tr)
}
