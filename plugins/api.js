import Page from '~/api/page'
import Show from '~/api/show'
import Dates from '~/api/dates'
import App from '~/api/app'

export default (context, inject) => {
  // Initialize API factories
  const factories = {
    page: Page(context.$axios, context.store),
    show: Show(context.$axios, context.store),
    dates: Dates(context.$axios, context.store),
    app: App(context.$axios, context.store),
  }

  // Inject $api
  inject('api', factories)
}
