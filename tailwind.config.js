/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */

module.exports = {
  theme: {
    // Docs : https://tailwindcss.com/docs/theme/#theme-structure
    screens: {
      sm: '640px',
      // => @media (min-width: 640px) { ... }

      md: '768px',
      // => @media (min-width: 768px) { ... }

      lg: '1024px',
      // => @media (min-width: 1024px) { ... }

      xl: '1280px',
      // => @media (min-width: 1280px) { ... }

      xxl: '1920px',
      // => @media (min-width: 1920px) { ... }
    },
    // Docs: https://tailwindcss.com/docs/font-family/#customizing
    fontFamily: {
      sans: ['Barlow', 'sans-serif'],
    },
    fontWeight: {
      light: 300,
      regular: 400,
      medium: 600,
      bold: 700,
    },

    // Docs: https://tailwindcss.com/docs/customizing-colors/#app
    colors: {
      white: '#FFFFFF',
      black: '#444648',
      grey: '#cccccc',
      'grey-darker': '#999999',
      transparent: 'transparent',
      current: 'currentColor',
    },

    extend: {
      opacity: {
        10: '0.1',
        15: '0.15',
        20: '0.2',
        95: '0.95',
      },
      fontSize: {
        // h1: '1.25rem',
        // h1: '1.25rem',
      },
    },
  },
  plugins: [require('@tailwindcss/aspect-ratio')],

  variants: {
    extend: {
      opacity: ['hover', 'active', 'group-hover'],
      textColor: ['responsive', 'hover', 'focus', 'group-hover'],
      fill: ['hover', 'group-hover'],
    },
    aspectRatio: ['responsive', 'hover'],
  },

  //https://v1.tailwindcss.com/docs/upcoming-changes
  future: {
    purgeLayersByDefault: true,
  },
}
