// export default function (providerOptions) {
//     // Init provider

//     return {
//         // Absolute path to runtime file
//         runtime: require.resolve('./runtime'),

//         // Public options to use in runtime
//         runtimeOptions: providerOptions,

//         // A server middleware to optimize images
//         // This propery is optional you can omit this if don't need it
//         // middleware: createServerMiddleware(),
//     }
// }

// import { joinURL } from 'ufo'
// import {} from '~image'

// export function getImage(src, { modifiers, baseURL } = {}) {
export function getImage(
  src,
  { modifiers, baseURL } = {},
  { options, nuxtContext, $img }
) {
  const { width, height, quality, fit } = modifiers
  const operations = []

  if (width) operations.push(`width=${width}`)
  if (height) operations.push(`height=${height}`)
  if (fit) operations.push(`fit=${fit}`)
  if (quality) operations.push(`quality=${quality}`)
  const operationsString = operations.join('&')
  const url = `${src}?${operationsString}`
  //   console.log(url)
  return {
    url,
    isStatic: true,
  }
}
