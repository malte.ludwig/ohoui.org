export default {
  getImage(src, modifiers, options) {
    const { width, height, quality, fit } = modifiers
    const operations = []

    // console.log('runtime', width, height, quality, fit, options, src)

    if (width) operations.push(`width=${width}`)
    if (height) operations.push(`height=${height}`)
    if (fit) operations.push(`fit=${fit}`)
    if (quality) operations.push(`quality=${quality}`)

    const operationsString = '?' + operations.join('&')
    return {
      url: src + operationsString,
      static: false,
      getInfo: () => {
        return {
          width,
          height,
          bytes: 1,
        }
      },
    }
  },
}
