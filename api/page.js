export default ($axios, $store) => ({
  getPageBySlug(slug) {
    const request = slug
      ? `/items/pages?fields=*.*,*.directus_files_id.*&filter[slug]=${slug}`
      : `/items/pages?fields=*.*,*.directus_files_id.*&filter[slug][_empty]=true`
    return $axios.$get(request)
  },
  getPageByName(name) {
    const request = `/items/pages?fields=*.*,*.directus_files_id.*&filter[name]=${name}`
    return $axios.$get(request)
  },
})
