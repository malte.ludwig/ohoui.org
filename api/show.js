export default ($axios, $store) => ({
  getShow(slug) {
    const request = `/items/shows?fields=*.*,*.directus_files_id.*&filter[slug]=${slug}&sort[dates]=date_end`
    return $axios.$get(request)
  },
  getShowList() {
    const request = `/items/shows?fields=*.*,*.directus_files_id.*`
    return $axios.$get(request)
  },
})
