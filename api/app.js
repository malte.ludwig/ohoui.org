export default ($axios, $store) => ({
  getTranslations() {
    return $axios.$get(`items/translations`)
  },
  getNavigation() {
    return $axios.$get(`items/pages?fields=*.*.*.*`)
    // ;`items/pages?fields=*.*.*.*&filter={ "status": { "_eq": "published" }}`
  },
})
