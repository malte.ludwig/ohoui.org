export default ($axios, $store) => ({
  datesByShow(slug) {
    const request = `/items/dates?fields=*.*&filter[show][slug]=${slug}&sort[dates]=date_start&filter[status]=published`
    return $axios.$get(request)
  },
  datesBySeason(seasonId) {
    const request = `/items/dates?fields=*.*.*.*&filter[season][id]=${seasonId}&sort[dates]=date_start&filter[status]=published`
    return $axios.$get(request)
  },
  datesBySeasonAndShow(seasonId, showId) {
    const request = `/items/dates?fields=*.*.*.*&filter[season][id]=${seasonId}&filter[show][id]=${showId}&sort[dates]=date_start&filter[status]=published`
    return $axios.$get(request)
  },
  allDates() {
    const request = `/items/dates?fields=*.*&limit-1&sort[dates]=date_start&filter[status]=published`
    return $axios.$get(request)
  },

  getSeasons() {
    const request = `/items/seasons?fields=*.*&limit-1&filter[status]=published`
    return $axios.$get(request)
  },
})
